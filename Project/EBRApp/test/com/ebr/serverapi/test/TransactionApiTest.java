package com.ebr.serverapi.test;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import com.ebr.bean.Transaction;
import com.ebr.serverapi.TransactionApi;


class TransactionApiTest {

    private TransactionApi api = new TransactionApi();

    @Test
    void testSaveTransaction() {
        Transaction res = api.saveTransaction("123456789", 10000, 2);
        assertEquals("Error in TransactionApi API!", "123456789", res.getBankAccount());
        assertEquals("Error in TransactionApi API!", 10000, (int)res.getDepositAmount());
        assertEquals("Error in TransactionApi API!", 2, res.getRentedBikeID());
    }

}
