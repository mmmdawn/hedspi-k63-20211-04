package com.ebr.serverapi.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.ebr.bean.Station;
import com.ebr.serverapi.StationApi;


class StationApiTest {
    private StationApi api = new StationApi();

    @Test
    void testGetAllBikes() {
        List<Station> list = api.getAllStations();
        assertEquals("Error in getStations API!", list.size(), 8);
    }

}
