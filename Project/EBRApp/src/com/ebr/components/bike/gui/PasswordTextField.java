package com.ebr.components.bike.gui;

import javafx.scene.control.TextField;

public class PasswordTextField extends TextField {

    public static final int PASSWORD_MIN_LEN = 6;
    
    
    public PasswordTextField() {
        this.setPromptText("Enter password");
    }
    
    
    public boolean isFilled() {
        if (this.getText().trim().isEmpty()) {
            return false;
        }
        return true;
    }
    
    public boolean isValid() {
        if (this.getText() == null) return false;
        
        String txt = this.getText();
        int len = txt.length();
        
        if (!this.isFilled()) return false;
        if (len < PASSWORD_MIN_LEN) return false;
        for (int i = 0; i < len; i++) {
            if (!Character.isDigit(txt.charAt(i))) return false;
        }
        
        return true;
    }
    
}
