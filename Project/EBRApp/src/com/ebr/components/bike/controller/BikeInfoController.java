package com.ebr.components.bike.controller;

import java.io.File;

import com.ebr.bean.Bike;
import com.ebr.bean.BikeType;
import com.ebr.components.bike.gui.CardNumberTextField;
import com.ebr.components.bike.gui.PasswordTextField;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class BikeInfoController {
	@FXML 
	private Label BikeId;
	
	@FXML 
	private Label BikeName;
	
	@FXML 
	private Label BikeType;
	
	@FXML 
	private Label DPrice;
	
	@FXML 
	private Label EPrice;
	
	@FXML 
	private Label Deposite;
	
    @FXML
    private CardNumberTextField cardNumberTxt;
    
    @FXML 
    private PasswordTextField passwordTxt;
    
    @FXML
    private Button RentBtn;
    
    @FXML
    private Button CancelBtn;
	
	public void setBikeInfo(Bike bike) {
		BikeId.setText(String.valueOf(bike.getId()));
		BikeName.setText(String.valueOf(bike.getName()));
	}	
	
	@FXML
	public void handleRentButtonAction (ActionEvent event) {
		
	}
	
	@FXML
	public void handleCancelButtonAction (ActionEvent event) {
		Stage stage = (Stage) CancelBtn.getScene().getWindow();
        stage.close();
	}
}
