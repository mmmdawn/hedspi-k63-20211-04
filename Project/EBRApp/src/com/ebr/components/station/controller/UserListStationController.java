package com.ebr.components.station.controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.ebr.bean.Station;
import com.ebr.components.abstractdata.controller.ADataPageController;
import com.ebr.components.station.controller.EditStationController;
import com.ebr.components.station.gui.SearchTextField;
import com.ebr.serverapi.StationApi;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;

public class UserListStationController extends ADataPageController<Station> implements Initializable{
	@FXML
    private SearchTextField searchTextInput;
	
	@FXML
	private Button searchBtn;
	
	@FXML 
	private TableView<Station> StationList;
	
	@FXML 
	private TableColumn<Station, Integer> Id;
	
	@FXML 
	private TableColumn<Station, String> Name;
	
	@FXML
	private TableColumn<Station, String> Address;
	
	@FXML
	private TableColumn<Station, Integer> Capacity;
	
	@FXML 
	private TableColumn<Station, Void> View;

	ObservableList<Station> stationsList;
	
 	@Override
	public List<Station> search(Map<String, String> searchParams) {
		return StationApi.singleton().getStations(searchParams);
	}
 	
 	@Override 
 	public void initialize(URL url, ResourceBundle rb) {
 		Id.setCellValueFactory(new PropertyValueFactory<Station, Integer>("id"));
 		Name.setCellValueFactory(new PropertyValueFactory<Station, String>("name"));
 		Address.setCellValueFactory(new PropertyValueFactory<Station, String>("address"));
 		Capacity.setCellValueFactory(new PropertyValueFactory<Station, Integer>("capacity"));
 		
 		Callback<TableColumn<Station, Void>, TableCell<Station, Void>> cellFactory = new Callback<TableColumn<Station, Void>, TableCell<Station, Void>>() {
             @Override
             public TableCell<Station, Void> call(final TableColumn<Station, Void> param) {
                 final TableCell<Station, Void> cell = new TableCell<Station, Void>() {
                     private final Button btn = new Button("View");
                     {
                         btn.setOnAction((ActionEvent event) -> {                       	 
//                        	 Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
//                       		FXMLLoader loader = new FXMLLoader();
//                       		loader.setLocation(getClass().getResource("/com/ebr/components/station/gui/EditStationPane.fxml"));
//                       		Parent editStationView;
//                      		try {
//                      			editStationView = loader.load();
//                      			Scene scene = new Scene(editStationView);
//                      			EditStationController controller = loader.getController();
//                      			
//                      			Station data = getTableView().getItems().get(getIndex());
//                      			controller.setStationInfo(data);
//                      			
//                      			stage.setScene(scene);
//                      		} catch (IOException e1) {
//                      			e1.printStackTrace();
//                      		}
                        	 
                        	 Station data = getTableView().getItems().get(getIndex());
                             System.out.println("selectedData: " + data.getName());
                             
                         });
                     }

                     @Override
                     public void updateItem(Void item, boolean empty) {
                         super.updateItem(item, empty);
                         if (empty) {
                             setGraphic(null);
                         } else {
                             setGraphic(btn);
                         }
                     }
                 };
                 return cell;
             }
         };
         View.setCellFactory(cellFactory);
        
        List<Station> stations = StationApi.singleton().getAllStations();
 		stationsList = FXCollections.observableList(stations);
 		StationList.setItems(stationsList);
 		
 		
// 		StationList.getColumns().add(Edit);
 	}
 	
// 	@FXML
//    public void handleSearchButtonAction(MouseEvent event) {                
//        // validate the fields
//        if (!this.searchTextInput.isFilled()) {
//            // show error
//            System.out.println("Not enter search field.\n");
//            return;
//        }      
//        
//        System.out.println(this.searchTextInput.getText());
//
//    }
}
