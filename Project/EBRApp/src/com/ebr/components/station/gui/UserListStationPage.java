package com.ebr.components.station.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class UserListStationPage extends Application{
    
    @Override
    public void start(Stage primaryStage){
		try {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("com/ebr/components/station/gui/UserListStation.fxml"));
        primaryStage.setTitle("EBR");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
		}catch(Exception e) {
			e.printStackTrace();
		}
    }

    public static void main(String[] args) {
        launch(args);
    }

}
