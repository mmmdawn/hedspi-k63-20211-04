package com.ebr.factory;

import com.ebr.bean.BikeType;
import com.ebr.components.abstractdata.controller.ADataPageController;
import com.ebr.components.abstractdata.gui.ADataPage;
import com.ebr.components.biketype.controller.BikeTypePageController;

public class AdminPageFactory {

	private static AdminPageFactory singleton = new AdminPageFactory();
	
	public static AdminPageFactory singleton() {
		return singleton;
	}
	
	private AdminPageFactory() {
	}
	
	public ADataPage createPage(String type) {
		switch (type) {
		case "biketype":
			ADataPageController<BikeType> controller = new BikeTypePageController();
			return controller.getDataPagePane();
		default:
			return null;
		}
	}
}
