package com.ebr.serverapi;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.bean.Transaction;
import com.ebr.serverapi.interfaces.ITransactionApi;

public class TransactionApi implements ITransactionApi {

    public static final String PATH = "http://localhost:8080/";
    
    private static ITransactionApi singleton = new TransactionApi();
    public static ITransactionApi singleton() {
        return singleton;
    }

    public TransactionApi() {
    }

    @Override
    public Transaction saveTransaction(String cardNumber, float depositAmount, int rentedBikeID) {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(PATH).path("transactions/add")
                                                 .queryParam("cardNumber", cardNumber)
                                                 .queryParam("depositAmount", depositAmount)
                                                 .queryParam("rentedBikeID", rentedBikeID);
        
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        Transaction res = response.readEntity(new GenericType<Transaction>(){});
        return res;
    }

}
