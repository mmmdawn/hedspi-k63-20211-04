package com.ebr.serverapi.interfaces;

import java.util.ArrayList;

import com.ebr.bean.Bike;

public interface IBikeApi {
    
    public ArrayList<Bike> getAllBikes();
    public ArrayList<Bike> getAllBikesAtStation(int stationId);
    
    public Bike returnBike(int bikeId, int dockStationId);
    
}
