package com.ebr.subsystem.bankapi.interfaces;

public interface IBankApi {

    public String increaseBankBalance(String cardNumber, String password, long amount);
    
    public String decreaseBankBalance(String cardNumber, String password, long amount);
    
    public String checkBankBalanceEnough(String cardNumber, String password, long amount);
    
}
