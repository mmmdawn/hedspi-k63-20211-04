package com.ebr.db;

import java.util.ArrayList;

import com.ebr.bean.Bike;
import com.ebr.bean.Station;
import com.ebr.bean.Transaction;
import com.ebr.db.seed.Seed;

public class JsonBikeDatabase implements IBikeDatabase {
	private static IBikeDatabase singleton = new JsonBikeDatabase();
	
	private ArrayList<Bike> bikes = Seed.singleton().getBikes();
	private ArrayList<Station> stations = Seed.singleton().getStations();
	private ArrayList<Transaction> transactions = Seed.singleton().getTransactions();
	
	private JsonBikeDatabase() {
	}
	
	public static IBikeDatabase singleton() {
		return singleton;
	}

	@Override
	public ArrayList<Bike> searchBike(Bike bike) {
		ArrayList<Bike> res = new ArrayList<Bike>();
		for (Bike b: bikes) {
			if (b.match(bike)) {
				res.add(b);
			}
		}
		return res;
	}

	@Override
	public Bike addBike(Bike bike) {
		for (Bike b: bikes) {
			if (b.equals(bike)) {
				return null;
			}
		}
		
		bikes.add(bike);
		return bike;
	}
	
	@Override
	public Bike updateBike(Bike bike) {
		for (Bike b: bikes) {
			if (b.equals(bike)) {
				bikes.remove(b);
				bikes.add(bike);
				return bike;
			}
		}
		return null;
	}

    @Override
    public ArrayList<Bike> searchBikeByType(Class<?> cls) {
        ArrayList<Bike> res = new ArrayList<Bike>();
        for (Bike b: bikes) {
            if (b.getClass().equals(cls)) {
                res.add(b);
            }
        }
        return res;
    }
	
    @Override
    public ArrayList<Bike> searchBikeAtStation(int dockStationId) {
        ArrayList<Bike> res = new ArrayList<Bike>();
        for (Bike b : bikes) {
            if (b.atDockStation(dockStationId)) {
                res.add(b);
            }
        }
        return res;
    }
    
    @Override
    public Bike returnBike(int bikeId, int dockStationId) {
        for (Bike b : bikes) {
            if (b.getId() == bikeId) {
                b.setStatus(0); // status -> isRented == 0
                b.setDockStationCode(dockStationId);
                
                // find station
                for (Station s : stations) {
                    if (s.getId() == dockStationId) {
                        s.setCapacity(s.getCapacity() + 1);
                    }
                }
                
                return b;
            }
        }
        return null;
    }
    
	
	@Override
    public ArrayList<Station> searchStation(Station station) {
        ArrayList<Station> res = new ArrayList<Station>();
        for (Station s: stations) {
            if (s.match(station)) {
                res.add(s);
            }
        }
        return res;
    }

    @Override
    public Station addStation(Station station) {
        for (Station s: stations) {
            if (s.equals(station)) {
                return null;
            }
        }
        
        stations.add(station);
        return station;
    }
    
    @Override
    public Station updateStation(Station station) {
        for (Station s: stations) {
            if (s.match(station)) {
                stations.remove(s);
                stations.add(station);
                return station;
            }
        }
        return null;
    }


	@Override
	public ArrayList<Transaction> searchTransaction(Transaction transaction) {
	    ArrayList<Transaction> res = new ArrayList<Transaction>();
        for (Transaction t: transactions) {
            if (t.match(transaction)) {
                res.add(t);
            }
        }
        return res;
	}
	
	@Override
    public Transaction addTransaction(Transaction transaction) {
        for (Transaction t : transactions) {
            if (t.equals(transaction)) {
                return null;
            }
        }
        transactions.add(transaction);
        return transaction;
    }

	@Override
	public boolean checkOutTransaction(Transaction transaction) {
		return false;
	}

}
