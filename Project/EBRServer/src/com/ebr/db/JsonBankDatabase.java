package com.ebr.db;

import java.util.ArrayList;

import com.ebr.bean.BankCard;
import com.ebr.db.seed.Seed;

public class JsonBankDatabase implements IBankDatabase {
    
    public static final String SUCCESS_RES = "1";
    public static final String BALANCE_NOT_ENOUGH_RES = "0";
    public static final String CARD_NUMBER_NOT_FOUND_RES = "-1";
    
    
    private static IBankDatabase singleton = new JsonBankDatabase();

    private ArrayList<BankCard> bankCards = Seed.singleton().getBankCards();

    public JsonBankDatabase() {
    }
    
    public static IBankDatabase singleton() {
        return singleton;
    }

    @Override
    public ArrayList<BankCard> searchBankCard(BankCard bankcard) {
        ArrayList<BankCard> res = new ArrayList<BankCard>();
        for (BankCard bc : bankCards) {
            if (bc.match(bankcard)) {
                res.add(bc);
            }
        }
        return res;
    }

    @Override
    public String increaseBankBalance(String cardNumber, String password, long amount) {
        for (BankCard bc : bankCards) {
            if (bc.match(cardNumber, password)) {
                bc.increaseBalance(amount);
                return SUCCESS_RES;
            }
        }
        return CARD_NUMBER_NOT_FOUND_RES;
    }

    @Override
    public String decreaseBankBalance(String cardNumber, String password, long amount) {
        for (BankCard bc : bankCards) {
            if (bc.match(cardNumber, password)) {
                String res = checkBankBalanceEnough(cardNumber, password, amount);
                if (!res.equals(SUCCESS_RES)) {
                    System.out.println("res: " + res + "\n");
                    return res;
                }
                bc.decreaseBalance(amount);
                return SUCCESS_RES;
            }
        }
        return CARD_NUMBER_NOT_FOUND_RES;
    }

    @Override
    public String checkBankBalanceEnough(String cardNumber, String password, long amount) {
        for (BankCard bc : bankCards) {
            if (bc.match(cardNumber, password)) {
                if (bc.balanceIsLargerOrEqual(amount)) {
                    return SUCCESS_RES;
                } else {
                    return BALANCE_NOT_ENOUGH_RES;
                }
            }
        }
        return CARD_NUMBER_NOT_FOUND_RES;
    }

}
