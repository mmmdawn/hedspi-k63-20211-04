package com.ebr.db;

import java.util.ArrayList;

import com.ebr.bean.Bike;
import com.ebr.bean.Station;
import com.ebr.bean.Transaction;

public interface IBikeDatabase {

    public Bike returnBike(int bikeId, int dockStationId);
    

	public ArrayList<Bike> searchBike(Bike bike);
	public Bike updateBike(Bike bike);
	public Bike addBike(Bike bike);
	public ArrayList<Bike> searchBikeByType(Class<?> cls);
    public ArrayList<Bike> searchBikeAtStation(int dockStationId);
    
	
	public ArrayList<Station> searchStation(Station station);
    public Station updateStation(Station station);
    public Station addStation(Station station);
	
    
	public ArrayList<Transaction> searchTransaction(Transaction transaction);
	public boolean checkOutTransaction(Transaction transaction);
	public Transaction addTransaction(Transaction transaction);
	
}
