package com.ebr.service;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.Bike;
import com.ebr.bean.TwinBike;
import com.ebr.db.IBikeDatabase;
import com.ebr.db.JsonBikeDatabase;

@Path("/")
public class TwinBikeService {
	
	private IBikeDatabase bikeDatabase;

	public TwinBikeService() {
		bikeDatabase = JsonBikeDatabase.singleton();
	}
	
	@GET
    @Path("twinbikes")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Bike> getEBikes(){
        ArrayList<Bike> res = bikeDatabase.searchBikeByType(TwinBike.class);
        return res;
    }

	@GET
	@Path("twinbikes/search")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Bike> getTwinBikes(@QueryParam("id") int id, @QueryParam("name") String name) {
		TwinBike twinbike = new TwinBike(id, name);
		ArrayList<Bike> res = bikeDatabase.searchBike(twinbike);
		return res;
	}
}

