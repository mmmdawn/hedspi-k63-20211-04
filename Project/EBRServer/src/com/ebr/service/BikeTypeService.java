package com.ebr.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.BikeType;
import com.ebr.db.IBikeTypeDatabase;
import com.ebr.db.JsonBikeTypeDatabase;

@Path("/")
public class BikeTypeService {
	
	private IBikeTypeDatabase biketypeDatabase;

	public BikeTypeService() {
		biketypeDatabase = JsonBikeTypeDatabase.singleton();
	}

	@GET
	@Path("biketypes")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<BikeType> getAllRentalCosts() {
		ArrayList<BikeType> res = biketypeDatabase.searchBikeType(null);
	    return res;
	}
	
	@POST
	@Path("biketypes/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BikeType updateRentalCost(@PathParam("id") String id, BikeType biketype) {
		return biketypeDatabase.updateRentalCost(biketype);
	}

}
