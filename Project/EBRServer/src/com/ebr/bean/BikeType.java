package com.ebr.bean;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("biketype")

public class BikeType {

	private String id;
	private String name;
	private int deposit;
	private String image; 
	private int defaultPrice;
	private int defaultRentalTime;
	private int extraRentalPrice; // tien tra them / 15 phut muon  
	private int earlyReturnBikeTime; //  tra xe truoc thoi gian nay se duoc hoan tien 
	private int refundAmount; // so tien duoc hoan lai neu tra som 

	public BikeType() {
		super();
	}
	
	public BikeType(String name, int deposit, int defaultPrice, int defaultRentalTime, int extraRentalPrice,
			int earlyReturnBikeTime, int refundAmount) {
		super();
		this.name = name;
		this.deposit = deposit;
		this.defaultPrice = defaultPrice;
		this.defaultRentalTime = defaultRentalTime;
		this.extraRentalPrice = extraRentalPrice;
		this.earlyReturnBikeTime = earlyReturnBikeTime;
		this.refundAmount = refundAmount;
	}
	
	public BikeType(String id, String name, String image, int deposit, int defaultPrice, int defaultRentalTime, int extraRentalPrice,
			int earlyReturnBikeTime, int refundAmount) {
		super();
		this.id = id;
		this.name = name;
		this.image = image;
		this.deposit = deposit;
		this.defaultPrice = defaultPrice;
		this.defaultRentalTime = defaultRentalTime;
		this.extraRentalPrice = extraRentalPrice;
		this.earlyReturnBikeTime = earlyReturnBikeTime;
		this.refundAmount = refundAmount;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getDeposit() {
		return deposit;
	}

	public void setDeposit(int deposit) {
		this.deposit = deposit;
	}

	public int getDefaultPrice() {
		return defaultPrice;
	}

	public void setDefaultPrice(int defaultPrice) {
		this.defaultPrice = defaultPrice;
	}

	public int getDefaultRentalTime() {
		return defaultRentalTime;
	}

	public void setDefaultRentalTime(int defaultRentalTime) {
		this.defaultRentalTime = defaultRentalTime;
	}

	public int getExtraRentalPrice() {
		return extraRentalPrice;
	}

	public void setExtraRentalPrice(int extraRentalPrice) {
		this.extraRentalPrice = extraRentalPrice;
	}

	public int getEarlyReturnBikeTime() {
		return earlyReturnBikeTime;
	}

	public void setEarlyReturnBikeTime(int earlyReturnBikeTime) {
		this.earlyReturnBikeTime = earlyReturnBikeTime;
	}

	public int getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(int refundAmount) {
		this.refundAmount = refundAmount;
	}
	
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	@Override
	public String toString() {
		return "id: " + this.id + ", title: " + this.name + ", image: " + this.image + ", deposit: " + this.deposit + ", defaultPrice: " + this.defaultPrice
				+ ", defaultRentalTime: " + this.defaultRentalTime + ", extraRentalPrice: " + this.extraRentalPrice + ", earlyReturnBikeTime: "+ this.earlyReturnBikeTime 
				+ ", refundAmount: " + this.refundAmount;
	}

	
	public boolean match(BikeType biketype) {
		
        if (biketype == null)
            return true;
        
        if (biketype.id != null && !biketype.id.equals("") && !this.id.contains(biketype.id)) {
            return false;
        }
        
        return true;
    }

}
