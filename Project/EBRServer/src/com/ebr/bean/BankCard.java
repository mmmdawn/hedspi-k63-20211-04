package com.ebr.bean;

public class BankCard {
    
    private String cardNumber;
    private String password;
    private long balance;

    
    public BankCard() {
    }
    
    public BankCard(String cardNumber, String password, long balance) {
        this.cardNumber = cardNumber;
        this.password = password;
        this.balance = balance;
    }


    public String getCardNumber() {
        return cardNumber;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public long getBalance() {
        return balance;
    }
    public void setBalance(long balance) {
        this.balance = balance;
    }
    public void increaseBalance(long amount) {
        this.balance += amount;
    }
    public void decreaseBalance(long amount) {
        this.balance -= amount;
    }
    
    
    public boolean balanceIsLargerOrEqual(long amount) {
        if (this.balance >= amount) {
            return true;
        }
        return false;
    }
    
    
    public boolean match(BankCard bankCard) {
        if (bankCard == null)
            return true;
        
        if (bankCard.cardNumber != null && !bankCard.cardNumber.equals("") && !this.cardNumber.equals(bankCard.cardNumber)) {
            return false;
        }
        
        if (bankCard.password != null && !bankCard.password.equals("") && !this.password.equals(bankCard.password)) {
            return false;
        }
        
        if (bankCard.balance < 0  && !(bankCard.balance == this.balance)) {
            return false;
        }
  
        return true;
    }
    
    
    public boolean match(String cardNumber, String password) {
        if (!this.cardNumber.equals(cardNumber)) {
            return false;
        }
        
        if (!this.password.equals(password)) {
            return false;
        }
        
        return true;
    }
   
}
