﻿Phân công nhiệm vụ:
- Phạm Công Minh: usecase thuê xe
- Ngô Bích Trang: usecase trả xe
- Phạm Lê Thanh Huyền: usecase thay đổi thông tin bãi xe
- Võ Hoàng Nam: usecase thay đổi giá xe

Nhật ký làm việc:
- Phạm Công Minh (10%)
    + Thiết kế usecase được phân công

- Ngô Bích Trang (30%)
    + Thiết kế usecase được phân công
    + Tạo server api cho Bike và banking api

- Phạm Lê Thanh Huyền (30%)
    + Thiết kế usecase được phân công
    + Tạo server api cho Station

- Võ Hoàng Nam (30%)
    + Thiết kế usecase được phân công
    + Tạo server api cho BikeType