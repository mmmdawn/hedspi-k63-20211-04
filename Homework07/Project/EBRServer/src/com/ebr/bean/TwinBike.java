package com.ebr.bean;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("TwinBike")
public class TwinBike extends Bike{

	public TwinBike() {
        super();
    }
	
	public TwinBike(int id, String name) {
        super(id, name);
    }

    public TwinBike(int id, String name, float weight, String image, int dockStationCode, int status) {
		super(id, name, weight, image, dockStationCode, status);
	}

    public TwinBike(int id, String name, float weight, String manuafacturingDate,
			String manuafacturingCompany, String image, int dockStationCode, int status, BikeType rentalCost) {
		super(id, name, weight, manuafacturingDate, manuafacturingCompany, image, dockStationCode, status, rentalCost);
	}

}
