package com.ebr.service;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.Transaction;
import com.ebr.db.IBikeDatabase;
import com.ebr.db.JsonBikeDatabase;

@Path("/")
public class TransactionService {
    
    private IBikeDatabase bikeDatabase;

    public TransactionService() {
        bikeDatabase = JsonBikeDatabase.singleton();
    }
    
    @GET
    @Path("transactions")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Transaction> getTransactions(){
        ArrayList<Transaction> res = bikeDatabase.searchTransaction(null);
        return res;
    }
    
    @GET
    @Path("transactions/search")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Transaction> getTransactions(@QueryParam("id") int id,
                                                  @QueryParam("bankAccount") String bankAccount){
        Transaction transaction = new Transaction(id, bankAccount);
        ArrayList<Transaction> res = bikeDatabase.searchTransaction(transaction);
        return res;
    }
    
    @GET
    @Path("transactions/add")
    @Produces(MediaType.APPLICATION_JSON)
    public Transaction getTransactions(@QueryParam("cardNumber") String cardNumber,
                                       @QueryParam("depositAmount") float depositAmount,
                                       @QueryParam("rentedBikeID") int rentedBikeID){
        Transaction transaction = new Transaction(cardNumber, depositAmount, rentedBikeID);
        Transaction res = bikeDatabase.addTransaction(transaction);
        return res;
    }

}
