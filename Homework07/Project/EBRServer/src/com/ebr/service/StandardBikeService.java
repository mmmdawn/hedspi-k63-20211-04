package com.ebr.service;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.Bike;
import com.ebr.bean.StandardBike;
import com.ebr.db.IBikeDatabase;
import com.ebr.db.JsonBikeDatabase;

@Path("/")
public class StandardBikeService {

	private IBikeDatabase bikeDatabase;
	
	public StandardBikeService() {
		bikeDatabase = JsonBikeDatabase.singleton();
	}
	
	@GET
    @Path("standardbikes")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Bike> getEBikes(){
        ArrayList<Bike> res = bikeDatabase.searchBikeByType(StandardBike.class);
        return res;
    }
	
	@GET
	@Path("standardbikes/search")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Bike> getStandardBikes(@QueryParam("id") int id, @QueryParam("name") String name) {
		StandardBike stdbike = new StandardBike(id, name);
		ArrayList<Bike> res = bikeDatabase.searchBike(stdbike);
		return res;
	}
}

