package com.ebr.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.Station;
import com.ebr.db.IBikeDatabase;
import com.ebr.db.JsonBikeDatabase;

@Path("/")
public class StationService {
    
    private IBikeDatabase bikeDatabase;
    
    public StationService() {
        bikeDatabase = JsonBikeDatabase.singleton();
    }
    
    @GET
    @Path("stations")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Station> getAllBikes() {
        ArrayList<Station> res = bikeDatabase.searchStation(null);
        return res;
    }
    
    @GET
    @Path("stations/search")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Station> getEBikes(@QueryParam("id") int id, @QueryParam("name") String name,
                                     @QueryParam("address") String address){
        Station station = new Station(id, name, address);
        ArrayList<Station> res = bikeDatabase.searchStation(station);
        return res;
    }
    
    @POST
	@Path("stations/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Station updateRentalCost(@PathParam("id") int id, Station station) {
		return bikeDatabase.updateStation(station);
	}
    
}
