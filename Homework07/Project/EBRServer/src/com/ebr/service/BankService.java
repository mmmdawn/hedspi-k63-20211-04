package com.ebr.service;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.BankCard;
import com.ebr.db.IBankDatabase;
import com.ebr.db.JsonBankDatabase;

@Path("/")
public class BankService {
    
    private IBankDatabase bankDatabase;
    
    public BankService() {
        bankDatabase = JsonBankDatabase.singleton();
    }
    
    // for testing only
    @GET
    @Path("bankcards")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<BankCard> _getAllBankCards() {
        ArrayList<BankCard> res = bankDatabase.searchBankCard(null);
        return res;
    }
    
    @GET
    @Path("bankcards/increaseBalance")
    @Produces(MediaType.TEXT_PLAIN)
    public String increaseBankBalance(@QueryParam("cardNumber") String cardNumber,
                                      @QueryParam("password") String password,
                                      @QueryParam("amount") long amount){
        String res = bankDatabase.increaseBankBalance(cardNumber, password, amount);
        return res;
    }
    
    @GET
    @Path("bankcards/decreaseBalance")
    @Produces(MediaType.TEXT_PLAIN)
    public String decreaseBankBalance(@QueryParam("cardNumber") String cardNumber,
                                      @QueryParam("password") String password,
                                      @QueryParam("amount") long amount){
        String res = bankDatabase.decreaseBankBalance(cardNumber, password, amount);
        return res;
    }
    
    @GET
    @Path("bankcards/checkBalanceIsEnough")
    @Produces(MediaType.TEXT_PLAIN)
    public String checkBalanceIsEnough(@QueryParam("cardNumber") String cardNumber,
                                        @QueryParam("password") String password,
                                        @QueryParam("amount") long amount){
        String res = bankDatabase.checkBankBalanceEnough(cardNumber, password, amount);
        return res;
    }

}
