package com.ebr.service;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.Bike;
import com.ebr.db.IBikeDatabase;
import com.ebr.db.JsonBikeDatabase;

@Path("/")
public class BikeService {
	
	private IBikeDatabase bikeDatabase;

	public BikeService() {
		bikeDatabase = JsonBikeDatabase.singleton();
	}

	@GET
	@Path("bikes")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Bike> getAllBikes() {
		ArrayList<Bike> res = bikeDatabase.searchBike(null);
	    return res;
	}
	
	@GET
    @Path("bikes/searchAtStation")
    @Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Bike> getAllBikesAtStation(@QueryParam("stationId") int stationId) {
        ArrayList<Bike> res = bikeDatabase.searchBikeAtStation(stationId);
        return res;
    }
	
	@GET
    @Path("bikes/returnBike")
    @Produces(MediaType.APPLICATION_JSON)
    public Bike returnBike(@QueryParam("bikeId") int bikeId,
                           @QueryParam("targetStationId") int targetStationId) {
        Bike res = bikeDatabase.returnBike(bikeId, targetStationId);
        return res;
    }
	
}
