package com.ebr.db;

import java.util.ArrayList;

import com.ebr.bean.BikeType;

public interface IBikeTypeDatabase {
	
	public ArrayList<BikeType> searchBikeType(BikeType biketype);
	public BikeType addBikeType(BikeType biketype);
	public BikeType updateRentalCost(BikeType biketype);
	
}
