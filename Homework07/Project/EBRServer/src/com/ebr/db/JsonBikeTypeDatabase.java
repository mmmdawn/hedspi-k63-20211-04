package com.ebr.db;

import java.util.ArrayList;

import com.ebr.bean.BikeType;
import com.ebr.db.seed.Seed;

public class JsonBikeTypeDatabase implements IBikeTypeDatabase{

    private static IBikeTypeDatabase singleton = new JsonBikeTypeDatabase();
	
	private ArrayList<BikeType> biketypes = Seed.singleton().getBikeTypes();
	
	private JsonBikeTypeDatabase() {
		
	}
	
	public static IBikeTypeDatabase singleton() {
		return singleton;
	}

	@Override
	public ArrayList<BikeType> searchBikeType(BikeType biketype) {
		ArrayList<BikeType> res = new ArrayList<BikeType>();
		for (BikeType r: biketypes) {
			if (r.match(biketype)) {
				res.add(r);
			}
		}
		return res;
	}

	@Override
	public BikeType addBikeType(BikeType biketype) {
		for (BikeType b: biketypes) {
			if (b.equals(biketype)) {
				return null;
			}
		}
		
		biketypes.add(biketype);
		return biketype;
	}
	
	@Override
	public BikeType updateRentalCost(BikeType biketype) {
		for (BikeType r: biketypes) {
			if (r.match(biketype)) {
				biketypes.remove(r);
				biketypes.add(biketype);
				return biketype;
			}
		}
		return null;
	}

}
