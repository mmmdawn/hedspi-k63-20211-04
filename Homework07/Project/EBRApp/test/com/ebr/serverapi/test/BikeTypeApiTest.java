package com.ebr.serverapi.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.ebr.bean.BikeType;
import com.ebr.serverapi.BikeTypeApi;

class BikeTypeApiTest {
	
	BikeTypeApi api = new BikeTypeApi();

	@Test
	void testGetAllBikeTypes() {
		List<BikeType> res =  api.getAllBikeTypes();
		List<BikeType> sample = new ArrayList<BikeType>();
		sample.add(new BikeType("EB1", "EBike", "src/com/ebr/components/biketype/image/ebike.png", 70000, 15000, 2, 5000, 1, 5000));
		sample.add(new BikeType("SD1", "Standard Bike", "src/com/ebr/components/biketype/image/standardbike.png", 60000, 10000, 2, 3000, 1, 5000));
		sample.add(new BikeType("TW1", "Twin Bike", "src/com/ebr/components/biketype/image/twinbike.png", 80000, 15000, 2, 5000, 1, 5000));
		assertEquals(res.get(0).getId(), sample.get(0).getId());
		assertEquals(res.get(1).getName(), sample.get(1).getName());
		assertEquals(res.get(2).getImage(), sample.get(2).getImage());
	}

	@Test
	void testGetBikeTypes() {
		Map<String, String> map = new HashMap<>();
		map.put("name", "EBike");
		List<BikeType> res =  api.getBikeTypes(map);
		List<BikeType> sample = new ArrayList<BikeType>();
		sample.add(new BikeType("EB1", "EBike", "src/com/ebr/components/biketype/image/ebike.png", 70000, 15000, 2, 5000, 1, 5000));
		assertEquals(res.get(0).getDeposit(), sample.get(0).getDeposit());
		assertEquals(res.get(0).getDefaultRentalTime(), sample.get(0).getDefaultRentalTime());
	}

}
