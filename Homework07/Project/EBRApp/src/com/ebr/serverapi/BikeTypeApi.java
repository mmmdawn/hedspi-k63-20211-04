package com.ebr.serverapi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.bean.BikeType;
import com.ebr.serverapi.interfaces.IBikeTypeApi;

public class BikeTypeApi implements IBikeTypeApi{

    public static final String PATH = "http://localhost:8080/";
    
    private static IBikeTypeApi singleton = new BikeTypeApi();
    
    public static IBikeTypeApi singleton() {
        return singleton;
    }
    
	public BikeTypeApi() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public List<BikeType> getAllBikeTypes() {
		Client client = ClientBuilder.newClient();
	    WebTarget webTarget = client.target(PATH).path("biketypes");

	    Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<BikeType> res = response.readEntity(new GenericType<ArrayList<BikeType>>() {});
		
	    return res;
	}
	
	@Override
	public List<BikeType> getBikeTypes(Map<String, String> queryParams) {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(PATH).path("biketypes");
				
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<BikeType> res = response.readEntity(new GenericType<ArrayList<BikeType>>() {});
		
		return res;
	}
	

	@Override
	public BikeType updateRentalCost(BikeType biketype) {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(PATH).path("biketypes").path(biketype.getId());
		System.out.println(webTarget);
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(biketype, MediaType.APPLICATION_JSON));
		
		BikeType res = response.readEntity(BikeType.class);
		System.out.println(res);
		return res;
	}
}
