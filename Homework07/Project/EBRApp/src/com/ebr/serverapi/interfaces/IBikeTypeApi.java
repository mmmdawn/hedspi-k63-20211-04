package com.ebr.serverapi.interfaces;

import java.util.List;
import java.util.Map;

import com.ebr.bean.BikeType;

public interface IBikeTypeApi {
	public List<BikeType> getAllBikeTypes();
	public List<BikeType> getBikeTypes(Map<String, String> queryStrings);
	public BikeType updateRentalCost(BikeType biketype);
}
