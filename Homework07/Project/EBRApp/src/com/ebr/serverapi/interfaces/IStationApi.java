package com.ebr.serverapi.interfaces;

import java.util.List;
import java.util.Map;

import com.ebr.bean.Station;

public interface IStationApi {
    public List<Station> getAllStations();
    public List<Station> getStations(Map<String, String> queryStrings);
    public Station updateStation(Station station);
}
