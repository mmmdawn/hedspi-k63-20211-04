package com.ebr.serverapi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.bean.Station;
import com.ebr.serverapi.interfaces.IStationApi;

public class StationApi implements IStationApi {
    
    public static final String PATH = "http://localhost:8080/";
    
    private static IStationApi singleton = new StationApi();
    public static IStationApi singleton() {
        return singleton;
    }
    
    public StationApi() {
    }
    
    @Override
    public ArrayList<Station> getAllStations() {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(PATH).path("stations");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>(){});
//        System.out.println(res);
        return res;
    }
    
    @Override
	public List<Station> getStations(Map<String, String> queryParams) {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(PATH).path("stations");
				
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>() {});
		
//		System.out.println(res);
		return res;
	}
    
    
    public Station updateStation(Station station) {
    	Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(PATH).path("stations").path(String.valueOf(station.getId()));
		System.out.println(webTarget);
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(station, MediaType.APPLICATION_JSON));
		
		Station res = response.readEntity(Station.class);
		System.out.println(res);
		return res;
    }
}