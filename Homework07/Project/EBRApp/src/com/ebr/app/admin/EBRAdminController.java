package com.ebr.app.admin;

import com.ebr.components.abstractdata.gui.ADataPage;
import com.ebr.factory.AdminPageFactory;

public class EBRAdminController {

	public EBRAdminController() {
		// TODO Auto-generated constructor stub
	}

	public ADataPage getPage(String type) {
		return AdminPageFactory.singleton().createPage(type);
	}
}
