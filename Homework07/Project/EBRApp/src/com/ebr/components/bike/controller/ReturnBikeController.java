package com.ebr.components.bike.controller;

import java.util.ArrayList;

import com.ebr.bean.Rental;
import com.ebr.bean.Station;
import com.ebr.serverapi.StationApi;

public class ReturnBikeController {
    
    private Rental currentRental;
    private Station targetSation;

    private StationApi stationApi = new StationApi();
    private ArrayList<Station> stations;
    
//    @FXML 
    
    
    public ReturnBikeController() {
    }
    
    public boolean checkBikeIsRented() {
        return Rental.isActive();
    }
    
    public boolean checkStationIsFull() {
        return this.targetSation.isFull();
    }

    public void endCurrentRental() {
        Rental.end();
    }
    
    
    public static void main(String[] args) {
    }

}
