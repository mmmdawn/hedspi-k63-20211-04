package com.ebr.components.bike.gui;

import java.util.ArrayList;

import com.ebr.bean.Station;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ReturnBikeScreen extends Application {
    
    private Station stations = new Station();
    

    public ReturnBikeScreen() {
    }

    @Override
    public void start(Stage primaryStage){
        try {
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("com/ebr/components/bike/gui/ReturnBike_StationList.fxml"));
            primaryStage.setTitle("EBR");
            primaryStage.setScene(new Scene(root));
            primaryStage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        launch(args);
    }

}
