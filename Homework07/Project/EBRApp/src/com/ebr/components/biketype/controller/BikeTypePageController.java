package com.ebr.components.biketype.controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.ebr.bean.BikeType;
import com.ebr.components.abstractdata.controller.ADataPageController;
import com.ebr.components.biketype.rentalcost.controller.RentalCostController;
import com.ebr.serverapi.BikeTypeApi;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;

public class BikeTypePageController extends ADataPageController<BikeType>{
	
	@FXML 
	private TableView<BikeType> BikeTypeList;
	
	@FXML 
	private TableColumn<BikeType, String> Id;
	
	@FXML 
	private TableColumn<BikeType, String> Name;
	
	@FXML
	private TableColumn<BikeType, Integer> DepositPrice;
	
	@FXML
	private TableColumn<BikeType, Integer> DefaultRentalPrice;
	
	@FXML
	private TableColumn<BikeType, Integer> DefaultRentalTime;
	
	@FXML
	private TableColumn<BikeType, Integer> ExtraRentalPrice;
	
	@FXML
	private TableColumn<BikeType, Integer> RefundAmount;
	
	@FXML
	private TableColumn<BikeType, Integer> EarlyReturnBikeTime;
	
	@FXML 
	private TableColumn<BikeType, Void> ChangePrice;

	ObservableList<BikeType> bikeTypesList;
	
 	@Override
	public List<BikeType> search(Map<String, String> searchParams) {
		return BikeTypeApi.singleton().getBikeTypes(searchParams);	
	}
 	
 	@Override 
 	public void initialize(URL url, ResourceBundle rb) {
 		Id.setCellValueFactory(new PropertyValueFactory<BikeType, String>("id"));
 		Name.setCellValueFactory(new PropertyValueFactory<BikeType, String>("name"));
 		DepositPrice.setCellValueFactory(new PropertyValueFactory<BikeType, Integer>("deposit"));
 		DefaultRentalPrice.setCellValueFactory(new PropertyValueFactory<BikeType, Integer>("defaultPrice"));
 		DefaultRentalTime.setCellValueFactory(new PropertyValueFactory<BikeType, Integer>("defaultRentalTime"));
 		ExtraRentalPrice.setCellValueFactory(new PropertyValueFactory<BikeType, Integer>("extraRentalPrice"));
 		RefundAmount.setCellValueFactory(new PropertyValueFactory<BikeType, Integer>("refundAmount"));
 		EarlyReturnBikeTime.setCellValueFactory(new PropertyValueFactory<BikeType, Integer>("earlyReturnBikeTime"));
 		
 		Callback<TableColumn<BikeType, Void>, TableCell<BikeType, Void>> cellFactory = new Callback<TableColumn<BikeType, Void>, TableCell<BikeType, Void>>() {
             @Override
             public TableCell<BikeType, Void> call(final TableColumn<BikeType, Void> param) {
                final TableCell<BikeType, Void> cell = new TableCell<BikeType, Void>() {
                private final Button btn = new Button("Change Price");
                     {
                         btn.setOnAction((ActionEvent event) -> {
                        	Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
                      		FXMLLoader loader = new FXMLLoader();
                      		loader.setLocation(getClass().getResource("/com/ebr/components/biketype/rentalcost/gui/change-rental-bike-screen.fxml"));
                      		Parent changeRentalPriceView;
                     		try {
                     			changeRentalPriceView = loader.load();
                     			Scene scene = new Scene(changeRentalPriceView);
                     			RentalCostController controller = loader.getController();
                     			BikeType biketype = getTableView().getItems().get(getIndex());
                     			controller.setBikeTypeInfo(biketype);
                     			stage.setScene(scene);
                     		} catch (IOException e1) {
                     			e1.printStackTrace();
                     		}
                         });
                     }
                     
                     @Override
                     public void updateItem(Void item, boolean empty) {
                         super.updateItem(item, empty);
                         if (empty) {
                             setGraphic(null);
                         } else {
                             setGraphic(btn);
                         }
                     }
                 };
                 return cell;
             }
         };
        ChangePrice.setCellFactory(cellFactory);
        
        List<BikeType> biketypes = BikeTypeApi.singleton().getAllBikeTypes();
 		bikeTypesList = FXCollections.observableList(biketypes);
 		
 		BikeTypeList.setItems(bikeTypesList);
 		
 	}

}
