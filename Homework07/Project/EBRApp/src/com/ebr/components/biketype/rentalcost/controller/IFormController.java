package com.ebr.components.biketype.rentalcost.controller;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public interface IFormController {
	
	public boolean validate(TextField inputParam, Label alertMsg, String type);

}
