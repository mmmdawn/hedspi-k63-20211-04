package com.ebr.components.invoice.gui;

import javafx.scene.control.TextField;

public class CardNumberTextField extends TextField {

    public static final int CARD_NUMBER_MAX_LEN = 14;
    public static final int CARD_NUMBER_MIN_LEN = 9;
    
    
    public CardNumberTextField() {
        this.setPromptText("Enter card number");
    }
    
    
    public boolean isFilled() {
        if (this.getText().trim().isEmpty()) {
            return false;
        }
        return true;
    }
    
    public boolean isValid() {
        if (this.getText() == null) return false;
        
        String txt = this.getText();
        int len = txt.length();
        
        if (!this.isFilled()) return false;
        if (len < CARD_NUMBER_MIN_LEN || len > CARD_NUMBER_MAX_LEN) return false;
        for (int i = 0; i < len; i++) {
            if (!Character.isDigit(txt.charAt(i))) return false;
        }
        
        return true;
    }
    
}
