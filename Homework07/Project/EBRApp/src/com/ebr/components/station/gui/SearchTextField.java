package com.ebr.components.station.gui;
import javafx.scene.control.TextField;

public class SearchTextField extends TextField {
    public SearchTextField() {
        this.setPromptText("Search a station");
    }
    
    public boolean isFilled() {
        if (this.getText().trim().isEmpty()) {
            return false;
        }
        return true;
    }
}

