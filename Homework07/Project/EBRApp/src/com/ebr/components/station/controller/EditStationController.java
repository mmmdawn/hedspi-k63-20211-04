package com.ebr.components.station.controller;

//import java.io.File;
import java.io.IOException;

import com.ebr.bean.Station;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import com.ebr.serverapi.StationApi;

public class EditStationController {
	
	public static final String LIST_PATH = "/com/ebr/components/station/gui/ListStation.fxml";
	public static final String POPUP_PATH = "/com/ebr/components/biketype/rentalcost/gui/success-pop-up.fxml";
	
	private int id;
	
	@FXML
	private TextField NameField;
	
	@FXML 
	private TextField AddressField;
	
	@FXML 
	private TextField CapacityField;
	
	public void setStationInfo(Station currentData) {
		id = currentData.getId();
		
		NameField.setText(String.valueOf(currentData.getName()));
		AddressField.setText(String.valueOf(currentData.getAddress()));
		CapacityField.setText(String.valueOf(currentData.getCapacity()));
	}

	
	@FXML
	public void handleSaveButtonAction (ActionEvent event) {
		String name = NameField.getText();
		String address = AddressField.getText();
		int capacity = Integer.parseInt(CapacityField.getText());
		
		Station station = new Station(id, name, address, capacity);
		StationApi.singleton().updateStation(station);
		
		Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
 		FXMLLoader loader = new FXMLLoader();
 		loader.setLocation(getClass().getResource(POPUP_PATH));
 		
 		Parent successPopup;
		try {
			successPopup = loader.load();
			Scene scene = new Scene(successPopup);
			stage.setScene(scene);
		} catch (IOException e1) {
			e1.printStackTrace();
		}	
	}
//	
//	@FXML
//	public void Submit (ActionEvent event) {
//		
//		Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
// 		FXMLLoader loader = new FXMLLoader();
// 		loader.setLocation(getClass().getResource(LIST_PATH));
// 		
// 		Parent bikeTypeListView;
//		try {
//			bikeTypeListView = loader.load();
//			Scene scene = new Scene(bikeTypeListView);
//			stage.setScene(scene);
//		} catch (IOException e1) {
//			e1.printStackTrace();
//		}	
//	}
//	
	@FXML
	public void handleCancelButtonAction (ActionEvent event) {
		Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
 		FXMLLoader loader = new FXMLLoader();
 		loader.setLocation(getClass().getResource(LIST_PATH));
 		Parent stationListView;
 		
		try {
			stationListView = loader.load();
			Scene scene = new Scene(stationListView);
			stage.setScene(scene);
		} catch (IOException e1) {
			e1.printStackTrace();
		}	
	}


}
