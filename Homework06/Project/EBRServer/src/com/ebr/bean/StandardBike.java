package com.ebr.bean;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("StandardBike")
public class StandardBike extends Bike{

	public StandardBike() {
        super();
    }
	
	public StandardBike(int id, String name) {
        super(id, name);
    }

    public StandardBike(int id, String name, float weight, String image, int dockStationCode, int status) {
		super(id, name, weight, image, dockStationCode, status);
	}

    public StandardBike(int id, String name, float weight, String manuafacturingDate,
			String manuafacturingCompany, String image, int dockStationCode, int status, BikeType rentalCost) {
		super(id, name, weight, manuafacturingDate, manuafacturingCompany, image, dockStationCode, status, rentalCost);
	}
	
}
