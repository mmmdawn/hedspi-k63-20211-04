package com.ebr.bean;


public class Transaction {
    
    private static int count = 0;
    
    private int id;
    private float depositAmount;
    private String startOfRentTime;
    private String endOfRentTime;
    private String bankAccount;
    private int rentedBikeID;
    
    
    public Transaction() {
        super();
    }

    public Transaction(int id, String bankAccount) {
        super();
        this.id = id;
        this.bankAccount = bankAccount;
    }

    public Transaction(float depositAmount, String startOfRentTime, String endOfRentTime,
            String bankAccount, String password, int rentedBikeID) {
        this.id = count++;
        this.depositAmount = depositAmount;
        this.startOfRentTime = startOfRentTime;
        this.endOfRentTime = endOfRentTime;
        this.bankAccount = bankAccount;
        this.rentedBikeID = rentedBikeID;
    }

    public Transaction(String cardNumber, float depositAmount, int rentedBikeID) {
        this.id = count++;
        this.depositAmount = depositAmount;
        this.startOfRentTime = "";
        this.endOfRentTime = "";
        this.bankAccount = cardNumber;
        this.rentedBikeID = rentedBikeID;
    }
    

    public int getId() {
        return id;
    }

    public float getDepositAmount() {
        return depositAmount;
    }

    public String getStartOfRentTime() {
        return startOfRentTime;
    }

    public String getEndOfRentTime() {
        return endOfRentTime;
    }

    public String getBankAccount() {
        return bankAccount;
    }
    
    public int getRentedBikeID() {
        return rentedBikeID;
    }
    

    public boolean match(Transaction transaction) {
        if (transaction == null) {
            return true;
        }
        
        if (this.id != transaction.id) {
            return false;
        }
        
        if (transaction.bankAccount != null && !transaction.bankAccount.equals("")) {
            return false;
        }
        
        return true;
    }
    
}
