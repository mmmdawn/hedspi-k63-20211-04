package com.ebr;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import com.ebr.service.BankService;
import com.ebr.service.BikeService;
import com.ebr.service.EBikeService;
import com.ebr.service.BikeTypeService;
import com.ebr.service.StandardBikeService;
import com.ebr.service.StationService;
import com.ebr.service.TransactionService;
import com.ebr.service.TwinBikeService;

public class EBRServer {
	public static final int PORT = 8080;
	
	public static void main(String[] args) throws Exception {
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");

		Server jettyServer = new Server(PORT);
		jettyServer.setHandler(context);

		ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
		jerseyServlet.setInitOrder(0);
		
		jerseyServlet.setInitParameter("jersey.config.server.provider.classnames",
				BikeService.class.getCanonicalName() + ", "  +
				EBikeService.class.getCanonicalName() + ", " +
				StandardBikeService.class.getCanonicalName()+ ", " +
				TwinBikeService.class.getCanonicalName() + ", " + 
				BikeTypeService.class.getCanonicalName() + ", " + 
				StationService.class.getCanonicalName() + ", " + 
				TransactionService.class.getCanonicalName() + ", " + 
				BankService.class.getCanonicalName()
		);

		try {
			jettyServer.start();
			jettyServer.join();
		} finally {
		    jettyServer.stop();
			jettyServer.destroy();
		}
	}

}

