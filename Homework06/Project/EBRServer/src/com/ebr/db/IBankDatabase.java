package com.ebr.db;

import java.util.ArrayList;

import com.ebr.bean.BankCard;

public interface IBankDatabase {
    public ArrayList<BankCard> searchBankCard(BankCard bankcard);
    
    public String increaseBankBalance(String cardNumber, String password, long amount);
    public String decreaseBankBalance(String cardNumber, String password, long amount);
    public String checkBankBalanceEnough(String cardNumber, String password, long amount);
}
