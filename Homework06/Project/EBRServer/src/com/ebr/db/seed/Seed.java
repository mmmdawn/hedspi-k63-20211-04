package com.ebr.db.seed;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.ebr.bean.BankCard;
import com.ebr.bean.Bike;
import com.ebr.bean.BikeType;
import com.ebr.bean.Station;
import com.ebr.bean.Transaction;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


public class Seed {
	private ArrayList<Bike> bikes;
	private ArrayList<BikeType> biketypes;
	private ArrayList<Station> stations;
	private ArrayList<Transaction> transactions;
    private ArrayList<BankCard> bankCards;
	
	private static Seed singleton = new Seed();
	
	private Seed() {
		start();
	}
	
	public static Seed singleton() {
		return singleton;
	}
	
	private void start() {
		bikes = new ArrayList<Bike>();
		bikes.addAll(generateBikeDataFromFile( new File(getClass().getResource("./standardbikes.json").getPath()).toString()));
		bikes.addAll(generateBikeDataFromFile( new File(getClass().getResource("./ebikes.json").getPath()).toString()));
		bikes.addAll(generateBikeDataFromFile( new File(getClass().getResource("./twinbikes.json").getPath()).toString()));
		
		biketypes = new ArrayList<BikeType>();
		biketypes.addAll(generateBikeTypeDataFromFile( new File(getClass().getResource("./biketypes.json").getPath()).toString()));
		
		stations = new ArrayList<Station>();
		stations.addAll(generateStationDataFromFile( new File(getClass().getResource("./stations.json").getPath()).toString()));
		
		transactions = new ArrayList<Transaction>();
		transactions.addAll(generateTransactionDataFromFile( new File(getClass().getResource("./transactions.json").getPath()).toString()));
	
		bankCards = new ArrayList<BankCard>();
		bankCards.addAll(generateBankCardDataFromFile( new File(getClass().getResource("./bankCards.json").getPath()).toString()));
	}
	
	private ArrayList<? extends Bike> generateBikeDataFromFile(String filePath){
		ArrayList<? extends Bike> res = new ArrayList<Bike>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileReader.read(filePath);
		
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<Bike>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		
		return res;
	}
	
	private ArrayList<BikeType> generateBikeTypeDataFromFile(String filePath){
		ArrayList<BikeType> res = new ArrayList<BikeType>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileReader.read(filePath);
		
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<BikeType>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		
		return res;
	}
	
	private ArrayList<? extends Station> generateStationDataFromFile(String filePath){
		ArrayList<? extends Station> res = new ArrayList<Station>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileReader.read(filePath);
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<Station>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		
		return res;
	}
	
	private ArrayList<? extends Transaction> generateTransactionDataFromFile(String filePath){
		ArrayList<? extends Transaction> res = new ArrayList<Transaction>();
		ObjectMapper mapper = new ObjectMapper();
		
		String json = FileReader.read(filePath);
		try {
			mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm"));
			res = mapper.readValue(json, new TypeReference<ArrayList<Transaction>>() { });
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}
		
		return res;
	}
	
	private ArrayList<? extends BankCard> generateBankCardDataFromFile(String filePath){
        ArrayList<? extends BankCard> res = new ArrayList<BankCard>();
        ObjectMapper mapper = new ObjectMapper();
        
        String json = FileReader.read(filePath);
        try {
            res = mapper.readValue(json, new TypeReference<ArrayList<BankCard>>() { });
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Invalid JSON input data from " + filePath);
        }
        
        return res;
    }

	public ArrayList<Bike> getBikes() {
		return bikes;
	}
	
	public ArrayList<BikeType> getBikeTypes() {
		return biketypes;
	}
	
	public ArrayList<Station> getStations() {
        return stations;
    }
	
	public ArrayList<Transaction> getTransactions() {
        return transactions;
    }
	
	public ArrayList<BankCard> getBankCards() {
        return bankCards;
    }


	public static void main(String[] args) {
		Seed seed = new Seed();
		seed.start();
	}

}
