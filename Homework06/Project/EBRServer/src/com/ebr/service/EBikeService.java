package com.ebr.service;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.Bike;
import com.ebr.bean.EBike;
import com.ebr.db.IBikeDatabase;
import com.ebr.db.JsonBikeDatabase;

@Path("/")
public class EBikeService {
	
	private IBikeDatabase bikeDatabase;

	public EBikeService() {
		bikeDatabase = JsonBikeDatabase.singleton();
	}
	
	@GET
	@Path("ebikes")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Bike> getEBikes(){
        ArrayList<Bike> res = bikeDatabase.searchBikeByType(EBike.class);
        return res;
    }
	
	@GET
	@Path("ebikes/search")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Bike> getEBikes(@QueryParam("id") int id, @QueryParam("name") String name){
		EBike ebike = new EBike(id, name);
		ArrayList<Bike> res = bikeDatabase.searchBike(ebike);
		return res;
	}
	
}
