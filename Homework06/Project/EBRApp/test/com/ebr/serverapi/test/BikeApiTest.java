package com.ebr.serverapi.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import com.ebr.bean.Bike;
import com.ebr.bean.StandardBike;
import com.ebr.bean.Station;
import com.ebr.serverapi.BikeApi;

class BikeApiTest {
    
    private static final int NUMBER_OF_BIKES = 25; // for testing :(
    
    private static final int STATION_456 = 456;
    private static final int NUMBER_OF_BIKES_AT_STATION_123 = 2;
    
    private BikeApi api = new BikeApi();

    @Test
    void testGetAllBikes() {
        ArrayList<Bike> list = api.getAllBikes();
        assertEquals("Error in getAllBikes API!", list.size(), NUMBER_OF_BIKES);
    }
    
    @Test
    void testGetAllBikesAtStation() {
        ArrayList<Bike> list = api.getAllBikesAtStation(STATION_456);
        assertEquals("Error in getAllBikesAtStation API!", list.size(), NUMBER_OF_BIKES_AT_STATION_123);
    }
    
    @Test
    void testReturnBike() {
        int testStationId = 7;
        int testBikeId = 9;
        Station station = new Station(testStationId, null, null);
        Bike bike = new StandardBike(testBikeId, "", 0, null, /*dockStationCode*/0, /*status*/1);
        
        Bike res = api.returnBike(bike.getId(), station.getId());

        assertEquals("Error in returnBike API - bike id", testBikeId, res.getId());
        assertEquals("Error in returnBike API - bike status", 0, res.getStatus());
    }

}
