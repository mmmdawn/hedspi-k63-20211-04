package com.ebr.subsystem.bankapi.test;

import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;

import com.ebr.subsystem.bankapi.BankApi;

class BankApiTest {
    
    private static final String TEST_CARD_NUMBER = "cardNumberForTesting"; // for testing :(
    private static final String TEST_CARD_ZERO_BALANCE = "zeroBalanceCard";
    
    private static final String TEST_CARD_PASSWORD = "123456";
    private static final String TEST_CARD_WRONG_PASSWORD = "12345";
    
    private BankApi api = new BankApi();
    private String res = "";
    
    @Test
    void testWrongPassword() {
        res = api.checkBankBalanceEnough(TEST_CARD_NUMBER, TEST_CARD_WRONG_PASSWORD, 0);
        assertEquals("Error in decreaseBankBalance() API!", BankApi.CARD_NUMBER_NOT_FOUND_RES, res);
    }

    @Test
    void testDecreaseBankBalance1() {
        res = api.decreaseBankBalance(TEST_CARD_NUMBER, TEST_CARD_PASSWORD, 500001);
        assertEquals("Error in decreaseBankBalance() API!", BankApi.BALANCE_NOT_ENOUGH_RES, res);
    }
    
    @Test
    void testDecreaseBankBalance2() {
        res = api.decreaseBankBalance(TEST_CARD_NUMBER, TEST_CARD_PASSWORD, 499999);
        assertEquals("Error in decreaseBankBalance() API!", BankApi.SUCCESS_RES, res);
        
        res = api.increaseBankBalance(TEST_CARD_NUMBER, TEST_CARD_PASSWORD, 499999);
        assertEquals("Error in increaseBankBalance() API!", BankApi.SUCCESS_RES, res);
    }
    
    @Test
    void testCheckBankBalanceEnough1() {
        res = api.decreaseBankBalance(TEST_CARD_NUMBER, TEST_CARD_PASSWORD, 499999);
        assertEquals("Error in decreaseBankBalance() API!", BankApi.SUCCESS_RES, res);
        
        res = api.checkBankBalanceEnough(TEST_CARD_NUMBER, TEST_CARD_PASSWORD, 2);
        assertEquals("Error in checkBankBalanceEnough() API!", BankApi.BALANCE_NOT_ENOUGH_RES, res);
    
        res = api.checkBankBalanceEnough(TEST_CARD_NUMBER, TEST_CARD_PASSWORD, 1);
        assertEquals("Error in checkBankBalanceEnough() API!", BankApi.SUCCESS_RES, res);
        
        res = api.checkBankBalanceEnough(TEST_CARD_NUMBER, TEST_CARD_PASSWORD, 0);
        assertEquals("Error in checkBankBalanceEnough() API!", BankApi.SUCCESS_RES, res); 
        
        res = api.increaseBankBalance(TEST_CARD_NUMBER, TEST_CARD_PASSWORD, 499999);
        assertEquals("Error in increaseBankBalance() API!", BankApi.SUCCESS_RES, res);
    }
    
    @Test
    void testIncreaseBankBalance() {
        res = api.checkBankBalanceEnough(TEST_CARD_ZERO_BALANCE , TEST_CARD_PASSWORD, 0);
        assertEquals("Error in checkBankBalanceEnough() API!", BankApi.SUCCESS_RES, res);
        
        res = api.checkBankBalanceEnough(TEST_CARD_ZERO_BALANCE , TEST_CARD_PASSWORD, 1);
        assertEquals("Error in checkBankBalanceEnough() API!", BankApi.BALANCE_NOT_ENOUGH_RES, res);
        
        res = api.increaseBankBalance(TEST_CARD_ZERO_BALANCE, TEST_CARD_PASSWORD, 10000);
        assertEquals("Error in increaseBankBalance() API!", BankApi.SUCCESS_RES, res);
        
        res = api.checkBankBalanceEnough(TEST_CARD_ZERO_BALANCE , TEST_CARD_PASSWORD, 10000);
        assertEquals("Error in checkBankBalanceEnough() API!", BankApi.SUCCESS_RES, res);

        res = api.decreaseBankBalance(TEST_CARD_ZERO_BALANCE, TEST_CARD_PASSWORD, 10001);
        assertEquals("Error in decreaseBankBalance() API!", BankApi.BALANCE_NOT_ENOUGH_RES, res);

        res = api.decreaseBankBalance(TEST_CARD_ZERO_BALANCE, TEST_CARD_PASSWORD, 10000);
        assertEquals("Error in decreaseBankBalance() API!", BankApi.SUCCESS_RES, res);
    }

}
