package com.ebr.bean.test;

import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;

import com.ebr.bean.Bike;
import com.ebr.bean.BikeType;
import com.ebr.bean.Rental;
import com.ebr.bean.StandardBike;

class RentalTest {

    @Test
    void testCalculatePureRent() {
        BikeType bikeType = new BikeType("SD2","StandardBike", /* deposit */ 700000,
                                            /* defaultPrice */ 10000,
                                            /* defaultRentalTime */ 30,
                                            /* extraRentalPrice */ 3000,
                                            /* earlyReturnBikeTime */ 1,
                                            /* rebate */ 5000);
        Bike bike = new StandardBike(0, "", 0, null, null, null, 0, 0, bikeType);
        
        Rental.start(bike);
        Rental._end(60 + 10); // endRental() for testing
        
        assertEquals("Error in CalculatePureRent()!", Rental.getPureRent(), 19000);
    }

}
