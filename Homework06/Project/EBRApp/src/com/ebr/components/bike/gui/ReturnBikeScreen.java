package com.ebr.components.bike.gui;

import com.ebr.bean.Bike;
import com.ebr.bean.BikeType;
import com.ebr.bean.Rental;
import com.ebr.bean.StandardBike;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ReturnBikeScreen extends Application {
    
    public ReturnBikeScreen() {
    }
    

    @Override
    public void start(Stage primaryStage){
		try {
	        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("com/ebr/components/bike/gui/ListStationToReturn.fxml"));
	        primaryStage.setTitle("EBR");
	        primaryStage.setScene(new Scene(root));
	        primaryStage.show();
		}catch(Exception e) {
			e.printStackTrace();
		}
    }
    
    
    public static void main(String[] args) {
    	BikeType rentalCost = new BikeType("", "Standard", /* deposit */ 700000,
		                /* defaultPrice */ 10000,
		                /* defaultRentalTime */ 30,
		                /* extraRentalPrice */ 3000,
		                /* earlyReturnBikeTime */ 1,
		                /* rebate */ 5000);
		Bike bike = new StandardBike(9, "StandardDX", 0, null, null, null, 7, 1, rentalCost);
		Rental.start(bike);
		Rental._end(15);
    	
        launch(args);
    }

}
