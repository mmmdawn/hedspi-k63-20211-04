package com.ebr.components.bike.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class MsgPopupController {
	
	@FXML
	private Button okBtn;
	@FXML
	private Button okBtn2;

	
	@FXML
	public void handleOkButtonAction(MouseEvent event) {
        Stage stage = (Stage) okBtn.getScene().getWindow();
        stage.close();
    }
	
	@FXML
	public void handleOkButtonAction2(MouseEvent event) {
        Stage stage = (Stage) okBtn2.getScene().getWindow();
        stage.close();
    }
	
}
