package com.ebr.components.biketype.rentalcost.controller;

import java.io.File;
import java.io.IOException;

import com.ebr.bean.BikeType;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import com.ebr.serverapi.BikeTypeApi;

public class RentalCostController implements IFormController{
	
	public static final String LIST_PATH = "/com/ebr/components/biketype/gui/list-bike-type-screen.fxml";
	public static final String POPUP_PATH = "/com/ebr/components/biketype/rentalcost/gui/success-pop-up.fxml";
	public static final String DEPOSIT_PRICE = "Deposit price";
	public static final String DEFAULT_RENTAL_PRICE = "Default rental price";
	public static final String DEFAULT_RENTAL_TIME = "Default rental time";
	public static final String EXTRA_RENTAL_PRICE = "Extra rental price";
	public static final String REFUND_AMOUNT = "Refund amount";
	public static final String EARLY_RETURN_TIME = "Early return bike time";
	
	private String id;
	private String imgPath;
	
	@FXML 
	private Label Name;
	
	@FXML
	private ImageView imageView;
	
	@FXML
	private TextField DepositPrice;
	
	@FXML 
	private TextField DefaultRentalPrice;
	
	@FXML 
	private TextField DefaultRentalTime;
	
	@FXML 
	private TextField ExtraRentalPrice;
	
	@FXML 
	private TextField RefundAmount;
	
	@FXML 
	private TextField EarlyReturnBikeTime;
	
	@FXML 
	private Label depositPriceAlertMsg;
	
	@FXML 
	private Label defaultRentalPriceAlertMsg;;

	@FXML 
	private Label defaultRentalTimeAlertMsg;

	@FXML 
	private Label extraRentalPriceAlertMsg;

	@FXML 
	private Label refundAmountAlertMsg;

	@FXML 
	private Label earlyReturnBikeTimeAlertMsg;

	
	public void setBikeTypeInfo(BikeType biketype) {
		id = biketype.getId();
		Name.setText(biketype.getName());
		DepositPrice.setText(String.valueOf(biketype.getDeposit()));
		DefaultRentalPrice.setText(String.valueOf(biketype.getDefaultPrice()));
		DefaultRentalTime.setText(String.valueOf(biketype.getDefaultRentalTime()));
		ExtraRentalPrice.setText(String.valueOf(biketype.getExtraRentalPrice()));
		RefundAmount.setText(String.valueOf(biketype.getRefundAmount()));
		EarlyReturnBikeTime.setText(String.valueOf(biketype.getEarlyReturnBikeTime()));
		System.out.println(biketype.getImage());
		imgPath = biketype.getImage();
		File file = new File(imgPath);
		Image image = new Image(file.toURI().toString());
		imageView.setImage(image);
	}

	
	@FXML
	public void handleChangeButtonAction (ActionEvent event) {
		
		String name = Name.getText();
		Integer count = 0;
		
		if(!validate(DepositPrice, depositPriceAlertMsg, DEPOSIT_PRICE)) {
			count++;
		}
		
		if(!validate(DefaultRentalTime, defaultRentalTimeAlertMsg, DEFAULT_RENTAL_TIME)) {
			count++;
		}
		
		if(!validate(DefaultRentalPrice, defaultRentalPriceAlertMsg, DEFAULT_RENTAL_PRICE)) {
			count++;
		}
		
		if(!validate(RefundAmount, refundAmountAlertMsg, REFUND_AMOUNT)) {
			count++;
		}
		
		if(!validate(ExtraRentalPrice, extraRentalPriceAlertMsg, EXTRA_RENTAL_PRICE)) {
			count++;
		}
		
		if(!validate(EarlyReturnBikeTime, earlyReturnBikeTimeAlertMsg, REFUND_AMOUNT)) {
			count++;
		}
			
		if(count == 0) {
			int depositPrice = Integer.parseInt(DepositPrice.getText());
			int defaultRentalPrice = Integer.parseInt(DefaultRentalPrice.getText());
			int defaultRentalTime = Integer.parseInt(DefaultRentalTime.getText());
			int extraRentalPrice = Integer.parseInt(ExtraRentalPrice.getText());
			int refundAmount = Integer.parseInt(RefundAmount.getText());
			int earlyReturnBikeTime = Integer.parseInt(EarlyReturnBikeTime.getText());
						
			BikeType biketype = new BikeType(id, name, imgPath, depositPrice, defaultRentalPrice, defaultRentalTime, extraRentalPrice, earlyReturnBikeTime, refundAmount);
			BikeTypeApi.singleton().updateRentalCost(biketype);
			
			Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
	 		FXMLLoader loader = new FXMLLoader();
	 		loader.setLocation(getClass().getResource(POPUP_PATH));
	 		
	 		Parent successPopup;
			try {
				successPopup = loader.load();
				Scene scene = new Scene(successPopup);
				stage.setScene(scene);
			} catch (IOException e1) {
				e1.printStackTrace();
			}	
		}

	}
	
	@FXML
	public void handleOkButtonAction (ActionEvent event) {
		
		Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
 		FXMLLoader loader = new FXMLLoader();
 		loader.setLocation(getClass().getResource(LIST_PATH));
 		
 		Parent bikeTypeListView;
		try {
			bikeTypeListView = loader.load();
			Scene scene = new Scene(bikeTypeListView);
			stage.setScene(scene);
		} catch (IOException e1) {
			e1.printStackTrace();
		}	
	}
	
	@FXML
	public void handleCancelButtonAction (ActionEvent event) {
		
		Stage stage = (Stage)((Node) event.getSource()).getScene().getWindow();
 		FXMLLoader loader = new FXMLLoader();
 		loader.setLocation(getClass().getResource(LIST_PATH));
 		Parent bikeTypeListView;
 		
		try {
			bikeTypeListView = loader.load();
			Scene scene = new Scene(bikeTypeListView);
			stage.setScene(scene);
		} catch (IOException e1) {
			e1.printStackTrace();
		}	
	}
	
	@Override 
	public boolean validate(TextField inputParam, Label alertMsg, String type) {
		inputParam.setStyle(null);
		alertMsg.setText("");
		if (inputParam.getText().length() == 0) {
			inputParam.setStyle("-fx-border-color: red; -fx-border-width:2px;");
			alertMsg.setText(type + " is empty!");	
			return false;
		}else {
			for (int i = 0; i < inputParam.getText().length(); i++) {
		        if (!Character.isDigit(inputParam.getText().charAt(i))) {
		        	inputParam.setStyle("-fx-border-color: red; -fx-border-width:2px;");
		    		alertMsg.setText(type + " must be positive integer!");	
		            return false;
		           }
			}
		}
		if (Integer.parseInt(inputParam.getText()) <= 0){
			inputParam.setStyle("-fx-border-color: red; -fx-border-width:2px;");
			alertMsg.setText(type + " must be than 0!");	
			return false;
		}		
		return true;
	}

}
