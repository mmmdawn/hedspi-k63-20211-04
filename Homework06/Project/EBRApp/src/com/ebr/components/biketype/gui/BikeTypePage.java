package com.ebr.components.biketype.gui;

import java.util.ArrayList;
import java.util.List;

import com.ebr.bean.BikeType;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class BikeTypePage extends Application  {
	
	public static final String PATH = "com/ebr/components/biketype/gui/list-bike-type-screen.fxml";

	@Override
    public void start(Stage primaryStage){
		try {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource(PATH));
        primaryStage.setTitle("EBR");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
		}catch(Exception e) {
			e.printStackTrace();
		}
    }
	
    public static void main(String[] args) {
    	
    	List<BikeType> demo = new ArrayList<BikeType>();
    	demo.add(new BikeType("EB1", "EBike", "src/com/ebr/components/biketype/image/ebike.png", 70000, 15000, 2, 5000, 1, 5000));
    	demo.add(new BikeType("SD1", "Standard Bike", "src/com/ebr/components/biketype/image/standardbike.png", 60000, 10000, 2, 3000, 1, 5000));
    	demo.add(new BikeType("TW1", "Twin Bike", "src/com/ebr/components/biketype/image/twinbike.png", 80000, 15000, 2, 5000, 1, 5000));
		System.out.println(demo);
        launch(args);
    }
}
