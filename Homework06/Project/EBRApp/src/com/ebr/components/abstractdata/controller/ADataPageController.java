package com.ebr.components.abstractdata.controller;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.ebr.components.abstractdata.gui.ADataPage;

import javafx.fxml.Initializable;

public abstract class ADataPageController<T> implements Initializable{
	
	private ADataPage pagePane;
	
	public ADataPageController() {
		
		
	}
		
	public ADataPage getDataPagePane() {
		return pagePane;
	}
		//ADataListPane<T> listPane = createListPane();
//		ADataSearchPane searchPane = ADataSearchPane();
//		searchPane.setController(new IDataSearchController() {
//			@Override
//			public void search(Map<String, String> searchParams) {
//				List<? extends T> list = ADataPageController.this.search(searchParams);
//				listPane.updateData(list);
//			}
//		});
//		
//		searchPane.fireSearchEvent();
//		
//		pagePane = new ADataPagePane<T>(searchPane, listPane);
//	}
	

	public abstract List<? extends T> search(Map<String, String> searchParams);
	
	@Override 
 	public void initialize(URL url, ResourceBundle rb) {
		
	}
	
}
