package com.ebr.components.abstractdata.gui;

import java.util.HashMap;
import java.util.Map;

import com.ebr.components.abstractdata.controller.IDataSearchController;


public abstract class ADataSearchPane {
	
	private IDataSearchController controller;
	
	public void setController(IDataSearchController controller) {
		this.controller = controller;
	}

	public Map<String, String> getQueryParams() {
		Map<String, String> res = new HashMap<String, String>();
		return res;
	}
	
	public void fireSearchEvent() {
		controller.search(getQueryParams());
	}

}
