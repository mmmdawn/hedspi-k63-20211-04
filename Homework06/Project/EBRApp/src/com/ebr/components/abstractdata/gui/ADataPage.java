package com.ebr.components.abstractdata.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public abstract class ADataPage extends Application {

	public ADataPage() {
		// TODO Auto-generated constructor stub
	}
	
	public static final String PATH = "com/ebr/components/biketype/gui/list-bike-type-screen.fxml";

	@Override
    public void start(Stage primaryStage){
		try {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource(PATH));
        primaryStage.setTitle("EBR");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
		}catch(Exception e) {
			e.printStackTrace();
		}
    }
	
    public void run() {
        launch();
    }

}
