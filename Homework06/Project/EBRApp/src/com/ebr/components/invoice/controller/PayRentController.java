package com.ebr.components.invoice.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.ebr.bean.Rental;
import com.ebr.bean.Station;
import com.ebr.components.invoice.gui.CardNumberTextField;
import com.ebr.components.invoice.gui.PasswordTextField;
import com.ebr.serverapi.BikeApi;
import com.ebr.serverapi.TransactionApi;
import com.ebr.subsystem.bankapi.BankApi;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class PayRentController implements Initializable {
    
    @FXML
    private Label bikeTypeLbl;
    @FXML
    private Label bikeNameLbl;
    @FXML
    private Label rentingTimeLbl;
    @FXML
    private Label depositLbl;
    @FXML
    private Label rentAmountLbl;
    @FXML
    private Label totalLbl;
    @FXML
    private CardNumberTextField cardNumberTxt;
    @FXML 
    private PasswordTextField passwordTxt;
    @FXML
    private Button payBtn;
    @FXML
    private Button cancelBtn;
    @FXML
    private Label errorTxt;
    
    
    public PayRentController() {
    }
    
    
    public void setStationInfo(Station data) {
		Rental.setReturnStationId(data.getId());
	}
    
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        setPaymentFormInfo();   
    }
    
    public void setPaymentFormInfo() {
        this.bikeTypeLbl.setText(Rental.getRentedBikeType());
        this.bikeNameLbl.setText(Rental.getRentedBikeName());
        
        String rentingTime = Long.toString(Rental.getRentingTimeInMinute());
        rentingTime += " minute(s)";
        this.rentingTimeLbl.setText(rentingTime);

        String deposit = Integer.toString(Rental.getDeposit());
        deposit += " VND";
        this.depositLbl.setText(deposit);
        
        String rentAmout = Integer.toString(Rental.getPureRent());
        rentAmout += " VND";
        this.rentAmountLbl.setText(rentAmout);
        
        String total = Integer.toString(Rental.getTotal());
        total += " VND";
        this.totalLbl.setText(total);
        
        errorTxt.setText(null);
    }
    
    @FXML
    public void handleCancelButtonAction(MouseEvent event) {
        Stage stage = (Stage) cancelBtn.getScene().getWindow();
        stage.close();
//        TODO: back to home screen
    }
    
    @FXML
    public void handlePayButtonAction(MouseEvent event) {                
        // validate the fields
        if (!this.cardNumberTxt.isValid() || !this.passwordTxt.isValid()) {
            // show error
            System.out.println("Card number or password is invalid!\n");
            showError("Error: Card number or password is invalid!");
            resetPaymentForm();
            return;
        }
        
        // all OK -> send the request to make transaction
        if (makeTransaction() == false) {
            showError("Error: Transacting failed. Please re-check your card info and balance");
            resetPaymentForm();
            return;
        } else {
        	showSuccess();
        }
        
        System.out.println("make transaction succeed!\n");
    }
    
    
    private void resetPaymentForm() {
        this.passwordTxt.setText(null);
    }
    
    public void showError(String errorMsg) {
        errorTxt.setText(errorMsg);
    }
    
    public void showSuccess() {
		try {
	    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/ebr/components/invoice/gui/SuccessMsgPopup.fxml"));
			Parent r = (Parent) loader.load();
			Scene scn = new Scene(r);
    	 	Stage stg = new Stage();
    	 	stg.setScene(scn);
    	 	stg.setTitle("");
    	 	stg.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    
    public boolean makeTransaction() {
        BankApi bankApi = new BankApi();
        int total = Rental.getTotal();
        String res = "";
        String cardNumber = this.cardNumberTxt.getText();
        String passwordTxt = this.passwordTxt.getText();
        
        showError("Preparing to making transaction...\n");
        
        if (total < 0) {
            res = bankApi.increaseBankBalance(cardNumber, passwordTxt, -total);
            System.out.println("increased\n");
        } else if (total > 0) {
            res = bankApi.checkBankBalanceEnough(cardNumber, passwordTxt, total);
            System.out.println("checked\n");
            
            if (res.equals(BankApi.SUCCESS_RES)) {
                res = bankApi.decreaseBankBalance(cardNumber, passwordTxt, total);
                System.out.println("decreased\n");
            } 
        } else {
            res = "no transaction made";
        }
        
        if (!res.equals(BankApi.SUCCESS_RES)) {
            return false;
        } else if (res.equals("no transaction made")) {
            // show noti
            System.out.println(res + "\n");
        }
        
        TransactionApi transactionApi = new TransactionApi();
        transactionApi.saveTransaction(cardNumber, total, Rental.getRentedBikeId());
        
        BikeApi bikeApi = new BikeApi();
        bikeApi.returnBike(Rental.getRentedBikeId(), Rental.getReturnStationId());
        
        return true;
    }

}
