package com.ebr.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = EBike.class, name = "EBike"),
                @Type(value = StandardBike.class, name = "StandardBike"),
                @Type(value = TwinBike.class, name = "TwinBike")})

public abstract class Bike {
    
    private int id;
    private String name;
    private float weight;
    private String manuafacturingDate;
    private String manuafacturingCompany;
    private String image;
	private int dockStationCode; 
	private int status;
	private BikeType rentalCost;
	
	public Bike() {
        super();
    }
	
	public Bike(int id, String name) {
        super();
        this.id = id;
        this.name = name;
    }
	
	public Bike(int id, String name, float weight,
            String image, int dockStationCode, int status) {
        super();
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.image = image;
        this.dockStationCode = dockStationCode;
        this.status = status;
    }
	
	public Bike(int id, String name, float weight,
	        String manuafacturingDate, String manuafacturingCompany,
            String image, int dockStationCode, int status, BikeType rentalCost) {
        super();
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.manuafacturingDate = manuafacturingDate;
        this.manuafacturingCompany = manuafacturingCompany;
        this.image = image;
        this.dockStationCode = dockStationCode;
        this.status = status;
        this.rentalCost = rentalCost;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public String getManuafacturingDate() {
        return manuafacturingDate;
    }

    public void setManuafacturingDate(String manuafacturingDate) {
        this.manuafacturingDate = manuafacturingDate;
    }

    public String getManuafacturingCompany() {
        return manuafacturingCompany;
    }

    public void setManuafacturingCompany(String manuafacturingCompany) {
        this.manuafacturingCompany = manuafacturingCompany;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getDockStationCode() {
		return dockStationCode;
	}

	public void setDockStationCode(int dockStationCode) {
		this.dockStationCode = dockStationCode;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public BikeType getRentalCost() {
		return rentalCost;
	}

	public void setRentalCost(BikeType rentalCost) {
		this.rentalCost = rentalCost;
	}

	
	public boolean match(Bike bike) {
        if (bike == null)
            return true;
        
        if (this.id != bike.id) {
            return false;
        }
        
        if (bike.name != null && !bike.name.equals("") && !this.name.contains(bike.name)) {
            return false;
        }
        
        return true;
    }

}
