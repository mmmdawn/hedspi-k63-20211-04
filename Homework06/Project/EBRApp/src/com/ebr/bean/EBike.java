package com.ebr.bean;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("Ebike")
public class EBike extends Bike {

	private int bateryPercentage;

	
	public EBike() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	public EBike(int id, String name) {
        super(id, name);
        // TODO Auto-generated constructor stub
    }

	
	public EBike(int id, String name, float weight, String image, int dockStationCode, int status) {
		super(id, name, weight, image, dockStationCode, status);
		// TODO Auto-generated constructor stub
	}

    public EBike(int id, String name, float weight, String manuafacturingDate,
            String manuafacturingCompany, String image, int dockStationCode, int status, BikeType rentalCost) {
        super(id, name, weight, manuafacturingDate, manuafacturingCompany, image, dockStationCode, status, rentalCost);
        // TODO Auto-generated constructor stub
    }

    public EBike(int id, String name, float weight, String manuafacturingDate,
			String manuafacturingCompany, String image, int dockStationCode, int status, BikeType rentalCost, int bateryPercentage) {
		super(id, name, weight, manuafacturingDate, manuafacturingCompany, image, dockStationCode, status, rentalCost);
		this.bateryPercentage = bateryPercentage;
	}
	
    
	public int getBateryPercentage() {
		return bateryPercentage;
	}

	public void setBateryPercentage(int bateryPercentage) {
		this.bateryPercentage = bateryPercentage;
	}
	
	@Override
	public boolean match(Bike bike) {
		if (bike == null)
			return true;
		
		boolean res = super.match(bike);
		if (!res) {
			return false;
		}
		
		if (!(bike instanceof EBike))
			return false;
		
		return true;
	}

}
