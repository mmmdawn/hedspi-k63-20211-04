package com.ebr.bean;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("station")

public class Station {
    public static final int MAX_CAPACITY = 70;
//	private static int count = 0;
	
	private int id;
	private String name;
	private String address;
	private int capacity;
	
	public Station() {
        super();
    }
	
	public Station(int id, String name, String address) {
    	super();
		this.id = id;
		this.name = name;
		this.address = address;
	}

    public Station(int id, String name, String address, int capacity) {
    	super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.capacity = capacity;
	}

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
		this.id = id;
	}
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getAddress() {
        return address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    
    public boolean match(Station station) {
        if (station == null)
            return true;
        
        if (this.id != station.id) {
            return false;
        }
        
        if (station.name != null && !station.name.equals("") && !this.name.contains(station.name)) {
            return false;
        }
        
        if (station.address != null && !station.address.equals("") && !this.address.contains(station.address)) {
            return false;
        }
        
        return true;
    }
    
//    public boolean isFull() {
//        if (this.capacity == Station.MAX_CAPACITY) {
//            return true;
//        }
//        return false;
//    }
}