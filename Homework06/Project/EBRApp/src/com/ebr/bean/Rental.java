package com.ebr.bean;

public class Rental {
    
    private static Bike rentedBike = null;
    private static int stationId = -1;
    
    private static long startTime = 0L;
    private static long endTime = 0L;
    
    private static int deposit = 0;
    private static int pureRent = 0;
    private static int total = 0;

    
    public Rental() {
    }
    
    public Rental(Bike rentedBike) {
        Rental.rentedBike = rentedBike;
        Rental.deposit = rentedBike.getRentalCost().getDeposit();
        Rental.startTime = (int) (System.currentTimeMillis() / 1000L);
    }
    
    public Rental(Bike rentedBike, int startTime, int endTime) {
        Rental.rentedBike = rentedBike;
        Rental.deposit = rentedBike.getRentalCost().getDeposit();
        Rental.startTime = startTime;
        Rental.endTime = endTime;
    }
    
    public static void setReturnStationId(int stationId) {
    	Rental.stationId = stationId;
    }
    public static Bike getRentedBike() {
        return rentedBike;
    }
    public static int getRentedBikeId() {
        return rentedBike.getId();
    }
    public static String getRentedBikeName() {
        return rentedBike.getName();
    }
    public static String getRentedBikeType() {
        return rentedBike.getClass().getSimpleName();
    }
    public static int getReturnStationId() {
        return stationId;
    }
    public static int getDeposit() {
        return deposit;
    }
    public static long getStartTime() {
        return startTime;
    }
    public static long getEndTime() {
        return endTime;
    }
    public static int getPureRent() {
        return pureRent;
    }
    public static int getTotal() {
        return total;
    }
    public static long getRentingTimeInMillisSecond() {
        return Rental.endTime - Rental.startTime;
    }
    public static long getRentingTimeInSecond() {
        return (long) (((float) Rental.getRentingTimeInMillisSecond()) / 1000.0);
    }
    public static long getRentingTimeInMinute() {
        return (long) (((float) Rental.getRentingTimeInSecond()) / 60.0 );
    }
    public static boolean isActive() {
        if (Rental.rentedBike == null) {
            return false;
        }
        return true;
    }
    
    
    public static void start(Bike rentedBike) {
        Rental.rentedBike = rentedBike;
        Rental.deposit = rentedBike.getRentalCost().getDeposit();
        Rental.startTime = System.currentTimeMillis() / 1000L;
    }
    
    public static void end() {
        Rental.endTime = System.currentTimeMillis() / 1000L;
        Rental.pureRent = calculatePureRent();
        Rental.total = Rental.pureRent - Rental.deposit; 
    }
    
    public static void _end(long rentingTimeInMinute) { // for testing
        Rental.startTime = 0;
        Rental.endTime = rentingTimeInMinute * 60 * 1000;
        Rental.pureRent = calculatePureRent();
        Rental.total = Rental.pureRent - Rental.deposit; 
    }
    
    
    public static int calculatePureRent() {
        BikeType rent = Rental.rentedBike.getRentalCost();
        long rentingTimeInMinute = Rental.getRentingTimeInMinute();
        float coefficient = 1f;
        
        // get coefficient
        if (!(Rental.rentedBike instanceof StandardBike)) {
            coefficient = 1.5f;
        }
        
        // get rent
        if (rentingTimeInMinute < rent.getEarlyReturnBikeTime()) {
            return 0;
        } else {
            int fee = rent.getDefaultPrice();
            
            if (rentingTimeInMinute > rent.getDefaultRentalTime()) {
                long extraTime = (long) Math.ceil((float)(rentingTimeInMinute - 30) / 15.0);
                fee += (extraTime * rent.getExtraRentalPrice());
            }
            
            return (int) ((float)fee * coefficient);
        }
    }

}
