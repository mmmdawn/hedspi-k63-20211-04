package com.ebr.subsystem.bankapi;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.subsystem.bankapi.interfaces.IBankApi;

public class BankApi implements IBankApi {

    public static final String PATH = "http://localhost:8080/bankcards/";
    public static final String INCREASE_BAlANCE_SERVICE = "increaseBalance";
    public static final String DECREASE_BAlANCE_SERVICE = "decreaseBalance";
    public static final String CHECK_BAlANCE_ENOUGH_SERVICE = "checkBalanceIsEnough";
    
    public static final String SUCCESS_RES = "1";
    public static final String BALANCE_NOT_ENOUGH_RES = "0";
    public static final String CARD_NUMBER_NOT_FOUND_RES = "-1";
    
    private static IBankApi singleton = new BankApi();
    public static IBankApi singleton() {
        return singleton;
    }

    public BankApi() {
    }

    @Override
    public String increaseBankBalance(String cardNumber, String password, long amount) {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(PATH).path(INCREASE_BAlANCE_SERVICE)
                                                 .queryParam("cardNumber", cardNumber)
                                                 .queryParam("password", password)
                                                 .queryParam("amount", amount);        

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.TEXT_PLAIN);
        Response response = invocationBuilder.get();

        String res = response.readEntity(new GenericType<String>(){});        
        return res;
    }

    @Override
    public String decreaseBankBalance(String cardNumber, String password, long amount) {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(PATH).path(DECREASE_BAlANCE_SERVICE)
                                                 .queryParam("cardNumber", cardNumber)
                                                 .queryParam("password", password)
                                                 .queryParam("amount", amount);        

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.TEXT_PLAIN);
        Response response = invocationBuilder.get();

        String res = response.readEntity(new GenericType<String>(){});        
        return res;
    }

    @Override
    public String checkBankBalanceEnough(String cardNumber, String password, long amount) {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(PATH).path(CHECK_BAlANCE_ENOUGH_SERVICE)
                                                 .queryParam("cardNumber", cardNumber)
                                                 .queryParam("password", password)
                                                 .queryParam("amount", amount);        

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.TEXT_PLAIN);
        Response response = invocationBuilder.get();

        String res = response.readEntity(new GenericType<String>(){});        
        return res;
    }

}
