package com.ebr.app.user;

import java.util.ArrayList;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.ebr.bean.Bike;
import com.ebr.serverapi.BikeApi;

public class EBRUser {

    public EBRUser() {
        BikeApi bikeApi = new BikeApi();
        ArrayList<Bike> res = bikeApi.getAllBikes();
        
        System.out.println(res.toString());
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new EBRUser();
            }
        });
        
    }
}
