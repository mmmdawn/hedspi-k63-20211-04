package com.ebr.serverapi;

import java.util.ArrayList;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.bean.Bike;
import com.ebr.serverapi.interfaces.IBikeApi;

public class BikeApi implements IBikeApi {

    public static final String PATH = "http://localhost:8080/";
    
    private static IBikeApi singleton = new BikeApi();
    public static IBikeApi singleton() {
        return singleton;
    }
    
    public BikeApi() {
    	
    }

    @Override
    public ArrayList<Bike> getAllBikes() {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(PATH).path("bikes");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        ArrayList<Bike> res = response.readEntity(new GenericType<ArrayList<Bike>>(){});
        System.out.println(res);
        return res;
    }
    
    @Override
    public ArrayList<Bike> getAllBikesAtStation(int stationId) {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(PATH).path("bikes/searchAtStation")
                                                 .queryParam("stationId", stationId);

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        ArrayList<Bike> res = response.readEntity(new GenericType<ArrayList<Bike>>(){});
        System.out.println(res);
        return res;
    }
    
    @Override
    public Bike returnBike(int bikeId, int dockStationId) {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(PATH).path("bikes/returnBike")
                                                 .queryParam("bikeId", bikeId)
                                                 .queryParam("stationId", dockStationId);

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        Bike res = response.readEntity(new GenericType<Bike>(){});
        return res;
    }

}
