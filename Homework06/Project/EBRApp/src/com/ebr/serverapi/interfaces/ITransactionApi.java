package com.ebr.serverapi.interfaces;

import com.ebr.bean.Transaction;

public interface ITransactionApi {

    public Transaction saveTransaction(String cardNumber, float depositAmount, int rentedBikeID);
    
}
